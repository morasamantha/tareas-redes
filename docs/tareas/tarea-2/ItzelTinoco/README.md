Topologia de Red
---------------------

![Topologia](/img/Topologia.jpeg)
Como se puede ver en la imgen se utilizo un Cloud PT para simular la salida a Internet, despues un Modem 1 que tomara la señal de internet y la conectara al Router 0, este a su vez distribuira la señal entre los 4 clientes que son 2 telefonos moviles, una laptop y una computadora de escritorio.
Los dos telefonos moviles y la laptom se conectan de manera inalambrica al router, mientras que la computadora de escritoria si cuenta con un cable ethernet.

Intente representar la division de las habitaciones con cuadrados en el modelo para que se entendiera mejor como estan distribuidos los dispositivos en mi casa.

Adjunto archivo pkt que contiene la topologia.
[Archivo pkt de la topologia](/files/Tarea2.pkt)

Tambien adjunto las capturas de pantalla de las direcciones IP estaticas de la computadora de escritorio y la Laptop.

![Ip de Pc1](/img/IPdePc.jpeg "IP de PC 1")
![Ip de Laptop 1](/img/IPdeLaptop.jpeg "IP de Laptop")
