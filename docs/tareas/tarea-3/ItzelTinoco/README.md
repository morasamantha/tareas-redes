# Itzel Tinoco

- Número de cuenta: `316020189`
- Usuario de GitLab: `@itzel-timmi`

Hola, esta es mi carpeta para la [*tarea-3*][liga-tarea-3].

Actividades a las que me quiero dedicar al salir de la facultad:

- Bases de Datos
- DevOps
- Desarrollo

Cosas que me gusta hacer en mi tiempo libre:

- Dormir
- Ver series
- Escuchar musica

| ![](img/imgrep.jpg)       |
|:---------------------------:|
| Esta es la imagen que elegí |
